"""tesis URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.10/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
#Este es el main de url, osea que este archivo reune las urls de tus apps
# Sorry por no poder hablar pero si nos conectamos mas temprano aulamos.



from django.conf.urls import include, url
from django.contrib import admin


admin.site.site_header = 'Escuela de Parvulos'

urlpatterns = [
	
    url(r'^admin/', admin.site.urls),
    url(r'^', include('tesis.apps.custom_auth.urls')), #<- aqui llamo la url de la app de custom_auth
    url(r'^', include('tesis.apps.productos.urls')), #<- aqui llamo la url de la app de productos
    url(r'^', include('tesis.apps.gestion.urls')), #<- aqui llamo la url de la app de gestion
]
