# -*- coding: utf-8 -*-
from django import forms
from django.forms import ModelForm
from django.contrib.auth.models import User
from .models import ModeloCategoriaProd, ModeloProductos

#Formulario Categorias
class CategoriaModelForm(forms.ModelForm):
    class Meta:
        model=ModeloCategoriaProd
        fields = ('categoria',)
        widgets = {
            'categoria' : forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Ingrese el nombre de la Categoría...',"required":"true",}),
            }

#Formulario Productos
class ProductoModelForm(forms.ModelForm):
    class Meta:
        model=ModeloProductos
        fields = ('nombre','categoria','cantidad')
        widgets = {
            'nombre' : forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Ingrese el nombre del Producto...',"required":"true",}),
            'categoria' : forms.Select(attrs={'class': 'form-control', 'placeholder': 'Seleccione la Categoría.',}),
            'cantidad' : forms.NumberInput(attrs={'class': 'form-control', "required":"true",}),
            }

