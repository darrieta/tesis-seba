# -*- encoding: utf-8 -*-
from django.shortcuts import render_to_response
from django.shortcuts import render
from django.contrib import messages

from django.template import RequestContext
from django.http import HttpResponse, HttpResponseRedirect
from django.contrib.auth import login, authenticate, logout, get_user_model
from django.views.decorators.cache import cache_page
from django.contrib.auth import get_user_model
from django.db.models import Sum, Count, Q
from django.contrib.auth.decorators import login_required
from django.contrib.sessions.models import Session
from datetime import date
from .forms import CategoriaModelForm, ProductoModelForm
import datetime
import socket
from .models import ModeloCategoriaProd, ModeloProductos

now = datetime.datetime.now()
hoy = now.strftime("%w")
tomorrow = now + datetime.timedelta(days=1)
today = date.today()

#Vista Categoria
def Categorias_view(request):
    if request.POST:
        form = CategoriaModelForm(request.POST)
        if form.is_valid():
            f = form.save(commit=False)
            f.save()
            messages.add_message(
                request,
                messages.INFO,
                'Cargo Guardado Exitosamente!'
                )
    form = CategoriaModelForm()
    ctx = {
        'categorias': ModeloCategoriaProd.objects.all(),
        'form': form
    }
    return render(request, 'app/categoria.html', ctx)

#Vista Borrar Categoria
def CategoriaDelete(request, pk):
    ModeloCategoriaProd.objects.get(pk=pk).delete()
    messages.add_message(
                request,
                messages.INFO,
                '¡Categoría Eliminada Exitosamente!'
                )
    return HttpResponseRedirect('/category/add/')

#Vista Actualizar Categoria
def ActualizarDatosCat(request, pk):
    categoria = ModeloCategoriaProd.objects.get(pk=pk)
    if request.method == "POST":
        form = CategoriaModelForm(request.POST, instance=categoria)
        if form.is_valid():
            form.save()
            messages.add_message(
                request,
                messages.INFO,
                '¡Categoría Editada Exitosamente!'
                )
            return HttpResponseRedirect('/category/add/')
    else:
        form = CategoriaModelForm(instance=categoria)
    ctx = {
        'categorias': ModeloCategoriaProd.objects.all(),
        'form': form
    }
    return render(request, 'app/cargar_categoria.html', ctx)
    #return render_to_response("/charge/add/",{"form":form},context_instance=RequestContext(request))

#Vista Productos
def Productos_view(request):
    if request.POST:
        form = ProductoModelForm(request.POST)
        if form.is_valid():
            f = form.save(commit=False)
            f.save()
            messages.add_message(
                request,
                messages.INFO,
                'Cargo Guardado Exitosamente!'
                )
    form = ProductoModelForm()
    ctx = {
        'productos': ModeloProductos.objects.all(),
        'form': form
    }
    return render(request, 'app/producto.html', ctx)

#Vista Borrar Producto
def ProductoDelete(request, pk):
    ModeloProductos.objects.get(pk=pk).delete()
    messages.add_message(
                request,
                messages.INFO,
                '¡Producto Eliminado Exitosamente!'
                )
    return HttpResponseRedirect('/product/add/')

#Vista Actualizar Productos
def ActualizarDatosProd(request, pk):
    producto = ModeloProductos.objects.get(pk=pk)
    if request.method == "POST":
        form = ProductoModelForm(request.POST, instance=producto)
        if form.is_valid():
            form.save()
            messages.add_message(
                request,
                messages.INFO,
                '¡Producto Editado Exitosamente!'
                )
            return HttpResponseRedirect('/product/add/')
    else:
        form = ProductoModelForm(instance=producto)
    ctx = {
        'productos': ModeloCategoriaProd.objects.all(),
        'form': form
    }
    return render(request, 'app/cargar_producto.html', ctx)
    #return render_to_response("/charge/add/",{"form":form},context_instance=RequestContext(request))

#Ejemplo Vistas
def ListadoProductosView(request):
    ctx = {'productos': ModeloProductos.objects.all()}
    return render(request, 'app/lista_productos.html', ctx)
