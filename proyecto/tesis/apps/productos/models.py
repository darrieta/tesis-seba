from __future__ import unicode_literals

from django.db import models

# Creamos los modelos (tablas)
# Lectura obligada: https://docs.djangoproject.com/es/1.10/ref/models/fields/

#Modelo Categoria de Productos
class ModeloCategoriaProd(models.Model):
	categoria = models.CharField(max_length=280, unique=True)
	
	def __unicode__(self):
		return self.categoria

	class Meta:
		unique_together=('categoria',)	

#Modelo Productos
class ModeloProductos(models.Model):
	nombre = models.CharField(max_length=250)
	cantidad = models.IntegerField(default=0)
	categoria = models.ForeignKey(ModeloCategoriaProd)
	
	def __unicode__(self):
		return self.nombre

	class Meta:
		unique_together=('nombre','cantidad', 'categoria',)
		ordering = ['nombre','cantidad','categoria']







