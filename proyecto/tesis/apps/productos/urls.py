__author__ = 'diegazo'

from django.conf.urls import include, url
from tesis.apps.productos import views

urlpatterns = [ # las urls son mas lindas en ingles 
	url(r'^product/list/$', views.ListadoProductosView, name='ProductList'), #Ejemplo Vista Productos
	url(r'^category/add/$', views.Categorias_view, name='crear-cat'), #Vista Crear Categoria
	url(r'^category/(?P<pk>\d+)/delete/$', views.CategoriaDelete, name='borrar-cat'), #Vista Borrar Categoria
    url(r'^category/(?P<pk>\d+)/cargar/$', views.ActualizarDatosCat, name='cargar-cat'), #Vista Actualizar Categoria
    url(r'^product/add/$', views.Productos_view, name='crear-prod'), #Vista Crear Productos
    url(r'^product/(?P<pk>\d+)/delete/$', views.ProductoDelete, name='borrar-prod'), #Vista Borrar Productos
    url(r'^product/(?P<pk>\d+)/cargar/$', views.ActualizarDatosProd, name='cargar-prod'), #Vista Actualizar Productos
]
                