from django.contrib import admin

# Registramos los modelos para poder llamarlos en el admin de django

from tesis.apps.productos.models import ModeloProductos, ModeloCategoriaProd


#Clase de atributos para modelo productos
class ProductoAdmin(admin.ModelAdmin):
    list_display = ('nombre','cantidad','categoria',) 	#Campos que quieres mostrar
    list_filter = ('categoria',)						#Campos con los que quieres filtrar
    search_fields = ['nombre', 'categoria__categoria',]	#Campos para buscar
    readonly_fields = ('cantidad',)



class CategoriaAdmin(admin.ModelAdmin):
    list_display = ('categoria',)
    


#Aqui registras los modelos (tablas) para que aparezcan en el admin
admin.site.register(ModeloCategoriaProd, CategoriaAdmin)
admin.site.register(ModeloProductos, ProductoAdmin)

