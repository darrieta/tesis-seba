from django.contrib import admin
from .models import ModeloPedido
from .models import ModeloAbast
from tesis.apps.productos.models import ModeloProductos, ModeloCategoriaProd
from django.contrib import messages

class PedidoAdmin(admin.ModelAdmin):
    '''
        Admin View for Pedido
    '''
    list_display = ('solicitante','producto','categoria','cantidad','fecha',)
    list_filter = ('fecha',)


class AbastAdmin(admin.ModelAdmin):
    '''
        Admin View for Abastecimiento
    '''
    list_display = ('solicitante','producto','categoria','cantidad','fecha',)
    list_filter = ('fecha',)




admin.site.register(ModeloPedido, PedidoAdmin)
admin.site.register(ModeloAbast, AbastAdmin)
