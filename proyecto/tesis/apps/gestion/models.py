# -*- encoding: utf-8 -*-

from __future__ import unicode_literals

from django.db import models
#Importamos el modelo de productos
from tesis.apps.custom_auth.models import UserProfile
from tesis.apps.productos.models import ModeloProductos, ModeloCategoriaProd
from django.utils import timezone

# Para enviar señales a otros modelos o apps
from django.db.models.signals import post_save
from django.dispatch import receiver

# Creamos las gestiones de SALIDA Y ENTRADA
# Segun convencion los nombres de los campos en los modelos deben ser
# escritos en minusculas separados por un guion bajo.
# https://docs.djangoproject.com/en/1.11/internals/contributing/writing-code/coding-style/#model-style

#Entrada
class ModeloAbast(models.Model):
	solicitante = models.ForeignKey(UserProfile)
	categoria = models.ForeignKey(ModeloCategoriaProd)
	producto = models.ForeignKey(ModeloProductos)
	cantidad = models.IntegerField()
	fecha = models.DateTimeField(auto_now_add=True)





#Salida
class ModeloPedido(models.Model):
	solicitante = models.ForeignKey(UserProfile)
	categoria = models.ForeignKey(ModeloCategoriaProd)
	producto = models.ForeignKey(ModeloProductos)
	cantidad = models.IntegerField()
	fecha = models.DateTimeField(auto_now_add=True)



"""
    def save(self):
    #    super(ModeloPedido, self).save()
    	 m = ModeloProducto.objects.get(pk=self.producto.pk)
    #    m.cantidad = m.cantidad - self.cantidad
    	 m.save()
    #    super(ModeloPedido, self).save()
"""

# Señaleticas ;)
# method for updating
@receiver(post_save, sender=ModeloAbast, dispatch_uid="actualizar_stock")
def update_stock(sender, instance, **kwargs):
    if kwargs.get('created', False):
        m = ModeloProductos.objects.get(pk=instance.producto.pk)
        m.cantidad = m.cantidad + instance.cantidad
        m.save()


@receiver(post_save, sender=ModeloPedido, dispatch_uid="actualizar_stock")
def update_stock(sender, instance, **kwargs):
    if kwargs.get('created', False):
        m = ModeloProductos.objects.get(pk=instance.producto.pk)
        m.cantidad = m.cantidad - instance.cantidad
        m.save()

# La logica del programa la podemos realizar aqui, donde se ingresan los datos
# Si se cargan articulos se suman
# se retiran se restan
# para esto se ocupa post_save es una funcion al final de cada clase del modelo
