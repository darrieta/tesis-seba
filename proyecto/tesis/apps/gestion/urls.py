__author__ = 'diegazo'

from django.conf.urls import include, url
from tesis.apps.gestion import views

urlpatterns = [ # las urls son mas lindas en ingles
	url(r'^management/catering/$', views.Abastecimiento_view, name='crear-abast'), #Vista Crear Categoria
	url(r'^management/ws/products/$', views.ws_productos, name='ws-productos'), #Vista Crear Categoria
	#url(r'^category/(?P<pk>\d+)/delete/$', views.CategoriaDelete, name='borrar-cat'), #Vista Borrar Categoria
    #url(r'^category/(?P<pk>\d+)/cargar/$', views.ActualizarDatosCat, name='cargar-cat'), #Vista Actualizar Categoria
]
