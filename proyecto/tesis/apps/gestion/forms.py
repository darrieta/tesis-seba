# -*- coding: utf-8 -*-
from django import forms
from django.forms import ModelForm
from django.contrib.auth.models import User
from .models import ModeloAbast

#Formulario Abastecimientos
class AbastModelForm(forms.ModelForm):
    class Meta:
        model=ModeloAbast
        fields = ('categoria', 'producto', 'cantidad',)
        widgets = {
            'producto' : forms.Select(attrs={'class': 'form-control', 'placeholder': 'Seleccione el Producto.',"required":"true", "readonly":"true",}),
            'categoria' : forms.Select(attrs={'class': 'form-control', 'placeholder': 'Seleccione la Categoría.',"required":"true",}),
            'cantidad' : forms.NumberInput(attrs={'class': 'form-control', 'default': '1' , "required":"true",}),
            }
