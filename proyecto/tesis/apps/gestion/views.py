# -*- encoding: utf-8 -*-
from django.shortcuts import render_to_response
from django.shortcuts import render
from django.contrib import messages
import json
from django.template import RequestContext
from django.http import HttpResponse, HttpResponseRedirect
from django.contrib.auth import login, authenticate, logout, get_user_model
from django.views.decorators.cache import cache_page
from django.contrib.auth import get_user_model
from django.db.models import Sum, Count, Q
from django.contrib.auth.decorators import login_required
from django.contrib.sessions.models import Session
from datetime import date
from .forms import AbastModelForm
import datetime
import socket
from .models import ModeloAbast
#Importamos modelos de la app productos
from tesis.apps.productos.models import ModeloCategoriaProd, ModeloProductos

#now = datetime.datetime.now()
#hoy = now.strftime("%w")
#tomorrow = now + datetime.timedelta(days=1)
#today = date.today()

#Vista Abastecimiento
def Abastecimiento_view(request):
    if request.POST:
        form = AbastModelForm(request.POST)
        if form.is_valid():
            f = form.save(commit=False)
            f.solicitante = request.user # Aqui es donde aplica el commit=false, antes de guardar completamos con los datos que queramos.
            f.save()
            messages.add_message(
                request,
                messages.INFO,
                'Abastecimiento Guardado Exitosamente!'
                )
    form = AbastModelForm()
    form.fields['producto'].queryset = ModeloAbast.objects.all()[:0]
    ctx = {
        'abastecimientos': ModeloAbast.objects.all()[:10],
        'form': form
    }
    return render(request, 'app/abastecimiento.html', ctx)

def ws_productos(request):
    id_Categoria = request.GET.get('id_categoria', False)
    results = []
    for dato in ModeloProductos.objects.filter(categoria__pk=id_Categoria):
        results.append({"id":dato.pk, "value": dato.nombre})
    data = json.dumps(results)
    mimetype = 'application/json'
    return HttpResponse(data, mimetype)
