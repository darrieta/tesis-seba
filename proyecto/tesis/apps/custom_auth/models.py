# -*- encoding: utf-8 -*-
import re
import time
import hashlib
from django.db import models
from django.contrib.auth import models as auth_models
import os
from binascii import hexlify

#Fotos
def respaldo(self, filename):
    ruta = "fotos_perfil/%s/erp_%s" % (self.rut, unicode(filename))
    return ruta

def _createHash():
    """
    """
    return hexlify(os.urandom(16))

PERFIL_USER = (
    ('administrador', 'Administrador'),
    ('caja', 'Caja'),
    ('ventas', 'Ventas'),
    ('bodega', 'Bodega'),
    ('gerente', 'Gerencia'),    
)

SEXO_CHOICES = (
    ('F', 'F'),
    ('M', 'M'),
    )

#Modelo Cargo
class ModeloCargo(models.Model):
    cargo = models.CharField(max_length=250, unique=True)
    
    def __unicode__(self):
        return self.cargo

    class Meta:
        unique_together=('cargo',)

#Modelo Crear Usuarios
class CustomUserManager(auth_models.BaseUserManager):
    def create_user(self, email, cargo, nombre, correo, password):

        user = self.model(
                            email   = CustomUserManager.normalize_email(email),
                            nombre  = nombre,
                            perfil  = perfil,
                            correo  = correo,
                            curso   = empresa,

                         )

        user.is_staff = True
        user.set_password(password)
        user.save(using = self._db)
        return user

    def create_superuser(self, email, password):
        user = self.model(
                            email  = CustomUserManager.normalize_email(email),

                         )

        user.is_staff = True
        user.is_superuser  = True
        user.set_password(password)
        user.save(using = self._db)
        return user


#Este es el modelo de usuarios, donde se encuentran los permisos etc, es un modelo abstracto.
# eso quiere decir que el modelo default de django (user) lo reemplazamos por este.

class UserProfile(auth_models.AbstractBaseUser, auth_models.PermissionsMixin):
    email = models.CharField(verbose_name='Usuario', max_length=100, unique=True)
    cargo = models.ForeignKey(ModeloCargo, blank=True, null=True)
    nombre = models.CharField(max_length=250, blank=True, null=True)
    fecha_nacimiento = models.DateField(blank=True, null=True)
    rut = models.CharField(max_length=10, blank=True, null=True)
    correo = models.CharField(max_length=100, blank=True, null=True)
    valid = models.BooleanField(default=False)
    counter = models.IntegerField(default=0)
    is_staff = models.BooleanField()
    is_active = models.BooleanField(default=True)
    hash = models.CharField(max_length=32, default=_createHash, unique=True)
    sexo = models.CharField(max_length=250, blank=True, null=True, choices=SEXO_CHOICES)
    last_login = models.DateTimeField(auto_now=True)
    USERNAME_FIELD = 'email'
    
    #REQUIRED_FIELDS = ['mobile', ]
    objects = CustomUserManager()
    def get_full_name(self):
        return self.email
    def get_short_name(self):
        return self.email
    def __unicode__(self):
        return unicode(self.nombre)
    def is_staff(self):
        return self.is_staff

