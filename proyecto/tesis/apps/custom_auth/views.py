# -*- encoding: utf-8 -*-
from django.shortcuts import render_to_response
from django.shortcuts import render
from django.contrib import messages

from django.template import RequestContext
from django.http import HttpResponse, HttpResponseRedirect
from django.contrib.auth import login, authenticate, logout, get_user_model
from django.views.decorators.cache import cache_page
from django.contrib.auth import get_user_model
from django.db.models import Sum, Count, Q
from django.contrib.auth.decorators import login_required
from django.contrib.sessions.models import Session
from datetime import date
from .forms import LoginForm, CargoModelForm, UsuarioModelForm, PerfilesModelForm
import datetime
import socket
from .models import ModeloCargo, UserProfile

now = datetime.datetime.now()
hoy = now.strftime("%w")
tomorrow = now + datetime.timedelta(days=1)
today = date.today()

# En esta parte me vuelvo autista buscando las pifias jajaja
# Esta vista es la que obliga al usuario a logearse, es el coladero de sesiones.


def porciento(cumplimiento, meta):
        return float(cumplimiento) * 100 / float(meta)


#Si revisas esta secuencia de verificacion te daras cuenta que es mas antiguo que la cresta!
# tiene muchas verificaciones lerdas
def Login_view(request):
    mensaje = ""
    user = request.user
    # Aqui pregunto si la sesion usuario se encuentra activa.
    if request.user.is_authenticated():
        return HttpResponseRedirect('/')
        # si eta registrado el usuario lo reenvio a la URL / que es la demo
    else:
        if request.method == "POST":
            form = LoginForm(request.POST)

            if form.is_valid():
                username = form.cleaned_data['username']
                password = form.cleaned_data['password']

                usuario = authenticate(username=username, password=password)
                if usuario is not None and usuario.is_active:
                    #request.session.set_expiry(999999999999)
                    login(request, usuario)

                    user = request.user
                    if user.valid == False:

                        user_info = get_user_model().objects.get(pk = usuario.id)
                        #ValidaSesion(usuario.pk)
                        user_info.valid = True
                        user_info.counter += 1
                        user_info.save()
                    else:
                        sesion = request.session.session_key
                        user_info = get_user_model().objects.get(pk = usuario.id)

                        user_info.counter += 1
                        user_info.save()



                    return HttpResponseRedirect('/')
                else:
                    mensaje = "Datos incorrectos para [ %s ]"%(username)
                    form = LoginForm()
                    ctx = {
                        'form':form,
                        'mensaje':mensaje,
                        }
                    return render(request, 'login/login.html', ctx)
            else:
                form = LoginForm()
                ctx = {
                    'form':form,
                    'mensaje':mensaje,
                    }
                return render(request, 'login/login.html', ctx)
        else:
            form = LoginForm() # Como llamo el form.py saco el formulario de ahi y lo envio al template como contexto.
            ctx = {
                'form':form,
                'mensaje':mensaje,
                }
            return render(request, 'login/login.html', ctx)

#Vista LogOut
def Salir_view(request):
    logout(request)
    return HttpResponseRedirect('/')


# Es solo el nombre del archivo, lo que pasa es que este es django 1.10 y el proyecto es 1.9
# puede ser eso, no se todavia

@login_required(login_url='/login/')
def demo_view(request):
    ctx = {'valor': None}
    return render(request, 'app/home.html', ctx)

# Vista Cargos
def Cargos_view(request):
    if request.POST:
        form = CargoModelForm(request.POST)
        if form.is_valid():
            f = form.save(commit=False)
            f.save()
            messages.add_message(
                request,
                messages.INFO,
                'Cargo Guardado Exitosamente!'
                )
    form = CargoModelForm()
    ctx = {
        'cargos': ModeloCargo.objects.all(),
        'form': form
    }
    return render(request, 'app/cargos.html', ctx)

#Vista Borrar Cargo
def CargoDelete(request, pk):
    ModeloCargo.objects.get(pk=pk).delete()
    messages.add_message(
                request,
                messages.INFO,
                '¡Cargo Eliminado Exitosamente!'
                )
    return HttpResponseRedirect('/charge/add/')

#Vista Actualizar Cargo
def ActualizarDatos(request, pk):
    cargo = ModeloCargo.objects.get(pk=pk)
    if request.method == "POST":
        form = CargoModelForm(request.POST, instance=cargo)
        if form.is_valid():
            form.save()
            messages.add_message(
                request,
                messages.INFO,
                '¡Cargo Editado Exitosamente!'
                )
            return HttpResponseRedirect('/charge/add/')
    else:
        form = CargoModelForm(instance=cargo)
    ctx = {
        'cargos': ModeloCargo.objects.all(),
        'form': form
    }
    return render(request, 'app/cargar_cargo.html', ctx)
    #return render_to_response("/charge/add/",{"form":form},context_instance=RequestContext(request))

#Vista Pantalla de Bloqueo
def LockScreen_view(request):
    mensaje = ""
    user = request.user
    # Aqui pregunto si la sesion usuario se encuentra activa.
    if request.user.is_authenticated():
        return HttpResponseRedirect('/')
        # si eta registrado el usuario lo reenvio a la URL / que es la demo
    else:
        if request.method == "POST":
            form = LoginForm(request.POST)

            if form.is_valid():
                username = form.cleaned_data['username']
                password = form.cleaned_data['password']

                usuario = authenticate(username=username, password=password)
                if usuario is not None and usuario.is_active:
                    #request.session.set_expiry(999999999999)
                    login(request, usuario)

                    user = request.user
                    if user.valid == False:

                        user_info = get_user_model().objects.get(pk = usuario.id)
                        #ValidaSesion(usuario.pk)
                        user_info.valid = True
                        user_info.counter += 1
                        user_info.save()
                    else:
                        sesion = request.session.session_key
                        user_info = get_user_model().objects.get(pk = usuario.id)

                        user_info.counter += 1
                        user_info.save()



                    return HttpResponseRedirect('/')
                else:
                    mensaje = "Datos incorrectos para [ %s ]"%(username)
                    form = LoginForm()
                    ctx = {
                        'form':form,
                        'mensaje':mensaje,
                        }
                    return render(request, 'login/login.html', ctx)
            else:
                form = LoginForm()
                ctx = {
                    'form':form,
                    'mensaje':mensaje,
                    }
                return render(request, 'login/login.html', ctx)
        else:
            form = LoginForm() # Como llamo el form.py saco el formulario de ahi y lo envio al template como contexto.
            ctx = {
                'form':form,
                'mensaje':mensaje,
                }
            return render(request, 'login/login.html', ctx)


# Vista Crear Usuarios
def Usuarios_view(request):
    if request.POST:
        form = UsuarioModelForm(request.POST)
        if form.is_valid():
            f = form.save(commit=False)
            f.save()
            messages.add_message(
                request,
                messages.INFO,
                'Usuario Guardado Exitosamente!'
                )
    form = UsuarioModelForm()
    ctx = {
        'usuarios': UserProfile.objects.all(),
        'form': form
    }
    return render(request, 'app/usuarios.html', ctx)

# Vista Perfiles
def Perfiles_view(request, hash):
    perfil = UserProfile.objects.get(hash=hash)
    if request.method == "POST":
        form = PerfilesModelForm(request.POST, instance=perfil)
        if form.is_valid():
            form.save()
            messages.add_message(
                request,
                messages.INFO,
                '¡Perfil Editado Exitosamente!'
                )
            return HttpResponseRedirect('/user/add/')
    else:
        form = PerfilesModelForm(instance=perfil)
    ctx = {
        'perfiles': UserProfile.objects.all(),
        'form': form
    }
    return render(request, 'app/perfiles.html', ctx)



# Crear usuarios, de la forma mas didactica.
def CrearUser_View(request):
    if request.POST:
        usuario = request.POST.get('usuario', False)
        if form.is_valid():
            form.save()
            messages.add_message(
                request,
                messages.INFO,
                '¡Usuario Creado Exitosamente!'
                )
            return HttpResponseRedirect('/user/add/')
    else:
        form = PerfilesModelForm(instance=perfil)
    ctx = {
        'perfiles': UserProfile.objects.all(),
        'form': form
    }
    return render(request, 'app/perfiles.html', ctx)

# Vista Directorio
def Directorio_view(request):
    return render(request, 'app/directorio.html')
