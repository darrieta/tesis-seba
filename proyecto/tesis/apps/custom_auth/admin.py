# -*- encoding: utf-8 -*-
#Este metodo abstracto me lo compartio un muy buen amigo de la India. #Favor cuidarlo como oro...
from django import forms
from django.contrib import admin
from django.contrib.auth.models import Group
from django.contrib.auth.admin import UserAdmin
from django.contrib.auth.forms import ReadOnlyPasswordHashField
from .models import UserProfile, ModeloCargo

class CargoAdmin(admin.ModelAdmin):
    list_display = ('cargo',)


class UserCreationForm(forms.ModelForm):
    password1 = forms.CharField(label='Password', widget=forms.PasswordInput)
    password2 = forms.CharField(label='Password confirmation', widget=forms.PasswordInput)

    class Meta:
        model = UserProfile
        fields = ('email', 'cargo','nombre',)

    def clean_password2(self):
        password1 = self.cleaned_data.get("password1")
        password2 = self.cleaned_data.get("password2")
        if password1 and password2 and password1 != password2:
            raise forms.ValidationError("Passwords don't match")
        return password2

    def save(self, commit=True):
        user = super(UserCreationForm, self).save(commit = False)
        user.set_password(self.cleaned_data["password1"])
        if commit:
            user.save()
            #print 'guarda'
            #print user
        #welcome(user.nombre, user.correo, user.email)
        return user


class UserChangeForm(forms.ModelForm):
    """A form for updating users. Includes all the fields on
    the user, but replaces the password field with admin's
    password hash display field.
    """
    password = ReadOnlyPasswordHashField()

    class Meta:
        model = UserProfile
        fields = ()

    def clean_password(self):
        # Regardless of what the user provides, return the initial value.
        # This is done here, rather than on the field, because the
        # field does not have access to the initial value
        return self.initial["password"]

def ValidFalse(modeladmin, request, queryset):
    queryset.update(is_active=False)
ValidFalse.short_description = 'is_active = False'

class UserProfileAdmin(UserAdmin):
    # The forms to add and change user instances
    form     = UserChangeForm
    add_form = UserCreationForm

    # The fields to be used in displaying the User model.
    # These override the definitions on the base UserAdmin
    # that reference specific fields on auth.User.
    list_display = ('email', 'nombre', 'correo','is_active','valid',)
    readonly_fields = ('hash',)
    list_filter  = ('is_superuser', 'is_active','valid','cargo',)
    actions = [ValidFalse,]
    fieldsets = (
        (None, {'fields': ('email','rut','nombre','fecha_nacimiento','sexo', 'password',)}),
        ('Propiedades: ', {'fields': ('cargo', 'valid','is_active','hash',)}),
        ('Permisos', {'fields': ('is_superuser', 'user_permissions', )}),
        
    )
    # add_fieldsets is not a standard ModelAdmin attribute. UserAdmin
    # overrides get_fieldsets to use this attribute when creating a user.
    add_fieldsets = (
        (None, {
            'classes' : ('wide',),
            'fields'  : ('email', 'nombre', 'rut', 'correo','password1', 'password2',)}
        ),
    )
    search_fields = ('email','nombre',)
    ordering = ('nombre',)
    filter_horizontal = ()


# Now register the new UserAdmin...
admin.site.register(UserProfile, UserProfileAdmin)
admin.site.register(ModeloCargo, CargoAdmin)
# ... and, since we're not using Django's builtin permissions,
# unregister the Group model from admin.
admin.site.unregister(Group)
"""
from django.contrib.sessions.models import Session
class SessionAdmin(ModelAdmin):
    def _session_data(self, obj):
        return obj.get_decoded()
    list_display = ['session_key', '_session_data', 'expire_date']
admin.site.register(Session, SessionAdmin)
"""