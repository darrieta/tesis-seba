# -*- coding: utf-8 -*-
from django import forms
from django.forms import ModelForm
from django.contrib.auth.models import User
from .models import UserProfile, ModeloCargo
from django.contrib.auth import login, authenticate, logout, get_user_model

# Formulario Login
class LoginForm(forms.Form):
    username = forms.CharField(label="Mucho", widget=forms.TextInput(attrs={"class":"form-control input-lg","required" : "True" , "placeholder" : "Usuario"}))
    password = forms.CharField(label="", widget=forms.PasswordInput(attrs={"class":"form-control input-lg",'required':'True','placeholder': 'Contraseña'},render_value=False))

#Formulario Cargo
class CargoModelForm(forms.ModelForm):
    class Meta:
        model=ModeloCargo
        fields = ('cargo',)
        widgets = {
            'cargo' : forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Ingrese el nombre del cargo...',"required":"true",}),
            }

#Formulario Pantalla Bloqueo
class LockScreenForm(forms.Form):
    password = forms.CharField(widget=forms.PasswordInput(attrs={"class":"form-control input-lg",'required':'True','placeholder': 'Contraseña'},render_value=False))

#Formulario Usuario
class UsuarioModelForm(forms.Form):
	username = forms.CharField(widget=forms.TextInput(attrs={"class":"form-control","required" : "True" , "placeholder" : "Usuario"}))
	password = forms.CharField(widget=forms.PasswordInput(attrs={"class":"form-control",'required':'True','placeholder': 'Contraseña'}))
	password1 = forms.CharField(widget=forms.PasswordInput(attrs={"class":"form-control",'required':'True','placeholder': 'Contraseña'}))

#Formulario Perfiles de Usuarios
class PerfilesModelForm(forms.ModelForm):
    class Meta:
        model=UserProfile
        fields = ('nombre','rut','fecha_nacimiento','cargo','correo','sexo','valid','is_active','hash',)
        widgets = {
           	'nombre' : forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Ingrese el nombre del Usuario.',"required":"true",}),
           	'rut' : forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Ingrese el rut del Usuario.',"required":"true",}),
           	'fecha_nacimiento' : forms.TextInput(attrs={'type':'date','class': 'form-control', 'placeholder': 'Ingrese la Fecha de Nacimiento',"required":"true",}),
           	'cargo' : forms.Select(attrs={'class': 'form-control', 'placeholder': 'Ingrese el nombre del cargo.',"required":"true",}),
        	'correo' : forms.EmailInput(attrs={'class': 'form-control', 'placeholder': 'Ingrese el Correo.',"required":"true",}),
        	'sexo' : forms.Select(attrs={'class': 'form-control', 'placeholder': 'Seleccione Sexo.',"required":"true",}),
    		'valid' : forms.CheckboxInput(attrs={'class': 'form-control',"required":"true",}),
    		#'is_staff' : forms.CheckboxInput(attrs={'class': 'form-control',"required":"true",}),
    		'is_active' : forms.CheckboxInput(attrs={'class': 'form-control',"required":"true",}),
    		'hash' : forms.TextInput(attrs={'class': 'form-control', "required":"true","readonly":"True",}),
    		}
