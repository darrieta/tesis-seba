__author__ = 'diegazo'

from django.conf.urls import include, url
from tesis.apps.custom_auth import views

urlpatterns = [
	url(r'^login/$', views.Login_view, name='vista_login'), #Vista Login
    url(r'^logout/$',views.Salir_view, name='vista_salir'), #Vista LogOut
    url(r'^lockScreen/$',views.LockScreen_view, name='vista_bloquear'), #Vista Bloquear Usuario
    url(r'^$', views.demo_view, name='vista_demo'), #Vista Demo
    url(r'^charge/add/$', views.Cargos_view, name='crear-cargo'), #Vista Crear Cargo
    url(r'^charge/(?P<pk>\d+)/delete/$', views.CargoDelete, name='borrar-cargo'), #Vista Borrar Cargo
    url(r'^charge/(?P<pk>\d+)/cargar/$', views.ActualizarDatos, name='cargar-cargo'), #Vista Actualizar Cargo
    url(r'^user/add/$', views.CrearUser_View, name='crear-user'), #Vista Crear User
    url(r'^user/(?P<hash>\w+)/profile/$', views.Perfiles_view, name='crear-perfil'), #Vista Crear User
    url(r'^directory/$', views.Directorio_view, name='vista-dir'), #Vista Directorio
]
